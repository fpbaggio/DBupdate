﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UpdateStruct
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void butselectCFG_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "*.Cfg|*.cfg";
            dlg.DefaultExt = "*.Cfg";
            dlg.InitialDirectory =@"D:\KingbosBZL";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                CmbCFGpath.Text = dlg.FileName;
                BLL.analysisCFG an = new BLL.analysisCFG();
                an.GetDBConnlist(CmbCFGpath.Text);
            }
        }

        private void InitCfgpath()
        {
            List<string> cfgllist = new UpdateStruct.Common.Files().FindFile(@"D:\");
            foreach (string s in cfgllist)
            {
                CmbCFGpath.Items.Add(s);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //InitCfgpath();
        }
    }
}
