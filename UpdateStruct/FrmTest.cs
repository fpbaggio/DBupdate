﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UpdateStruct.Common;
namespace UpdateStruct
{
    public partial class FrmTest : Form
    {
        UpdateStruct.DBstruct.StructHelper shelper;
        public FrmTest()
        {
            shelper = new DBstruct.StructHelper();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            //shelper.RebuiltTable(); 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(textBox2.Text))
                {
                    throw new Exception("请输入需要建立的表名");
                }
                shelper.RebuiltTable(textBox2.Text.ToUpper());
                MessageBox.Show("建立成功");
            }
            catch(Exception ex)
            {
                
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //将数据结构保存成list

            List<string> sList = StringPlus.GetStrlist(textBox1.Text , "*");
            foreach (string s in sList)
            {
                shelper.SaveToDB(s);
            }

            MessageBox.Show("保存成功！");
        }
    }
}
